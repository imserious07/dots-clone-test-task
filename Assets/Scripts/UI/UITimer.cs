﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[RequireComponent(typeof(TextMeshProUGUI))]
public class UITimer : MonoBehaviour
{
    private TextMeshProUGUI text;
    private LoseTimer timer;

    private void Awake() {
        timer = FindObjectOfType<LoseTimer>();
        text = GetComponent<TextMeshProUGUI>();

        if (timer != null) {
            timer.onCurrentTimeWasChanged += OnTimeChange;
        }

        OnTimeChange(timer.timeForLevel);
    }

    private void OnDestroy() {
        if (timer != null) {
            timer.onCurrentTimeWasChanged -= OnTimeChange;
        }
    }

    private void OnTimeChange(int currentTime) {
        text.text = currentTime.ToString();
    }
}