﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[RequireComponent(typeof(TextMeshProUGUI))]
public class UIScores : MonoBehaviour
{
    private TextMeshProUGUI text;
    private Scores scores;

    private void Awake() {
        text = GetComponent<TextMeshProUGUI>();

        if (scores == null) {
            scores = FindObjectOfType<Scores>();
        }

        if (scores != null) {
            scores.onScoresWasChanged += onScoresWasChanged;
        }

        onScoresWasChanged(scores.GetCurrentScores());
    }

    private void OnDestroy() {
        if (scores != null) {
            scores.onScoresWasChanged -= onScoresWasChanged;
        }
    }

    private void onScoresWasChanged(int scores) {
        text.text = "Scores: " + scores;
    }
}
