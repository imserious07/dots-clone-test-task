﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[RequireComponent(typeof(TextMeshProUGUI))]
public class UIHighScores : MonoBehaviour
{
    private Scores scores;
    private TextMeshProUGUI text;

    private void Awake() {
        text = GetComponent<TextMeshProUGUI>();

        if (scores == null) {
            scores = FindObjectOfType<Scores>();
        }

        if (scores != null) {
            text.text = "Record: " + scores.GetHighScores();
        }
    }
}
