﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIScreenLose : MonoBehaviour
{
    [SerializeField] private GameObject back;
    private LoseTimer timer;
    [SerializeField] private Button restartButton;

    private void Awake() {
        if (back != null) {
            back.gameObject.SetActive(false);
        }

        timer = FindObjectOfType<LoseTimer>();

        if (restartButton != null) {
            restartButton.onClick.AddListener(RestartButton);
        }

        if (timer != null) {
            timer.onTimeOut += OnTimesOut;
        }
    }

    private void OnDestroy() {
        if (timer != null) {
            timer.onTimeOut -= OnTimesOut;
        }
    }

    private void OnTimesOut() {
        back.gameObject.SetActive(true);
    }

    public void RestartButton() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}