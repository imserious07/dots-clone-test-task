﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "bubleGame/colors", fileName = "Colors", order = 0)]
public class Colors : ScriptableObject
{
    public List<ColorWithName> colors;

    public ColorWithName GetRandomColor() {
        if (colors.Count > 0) {
            return colors[Random.Range(0, colors.Count)];
        } else {
            return null;
        }
    }
}

[System.Serializable]
public class ColorWithName {
    public Color color;
    public string name;
}