﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Bubble))]
public class BubbleAnimations : MonoBehaviour
{
    [SerializeField] private Animator anim;
    [SerializeField] private Action<BubbleStates> onChangeAnimation;

    [ShowOnly]
    [SerializeField] private BubbleStates _currentState = BubbleStates.spawn;
    public BubbleStates currentState {
        set {
            ChangeAnimatorBoolean(value);

            _currentState = value;

            if (onChangeAnimation != null) {
                onChangeAnimation(currentState);
            }

            print(currentState);
        }
        get {
            return _currentState;
        }
    }

    private Bubble bubble;

    // Смена анимации для аниматора
    private void ChangeAnimatorBoolean(BubbleStates state) {
        if (currentState.Equals(state) == false) {
            ResetAllAnimations();

            print("State change to true: " + state.ToString());
            anim.SetBool(state.ToString(), true);

            StartCoroutine(cr_ResetAnimationsWithDelay(Time.fixedDeltaTime));
        }
    }

    private IEnumerator cr_ResetAnimationsWithDelay(float delay) {
        yield return new WaitForSeconds(delay);
        ResetAllAnimations();
    }

    private void ResetAllAnimations() {
        foreach (BubbleStates enumState in Enum.GetValues(typeof(BubbleStates))) {
            anim.SetBool(enumState.ToString(), false);
        }
    }

    private void Awake() {
        bubble = GetComponent<Bubble>();
        bubble.bubbleAnimations = this;

        currentState = BubbleStates.spawn;
    }

    public enum BubbleStates {
        spawn,
        idle_grow,
        idle,
        grow_up,
        grow_down,
        destroy
    }
}