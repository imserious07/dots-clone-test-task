﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoseTimer : MonoBehaviour
{
    [Tooltip("Время в секундах")]
    [SerializeField] public int timeForLevel = 60;

    private int _currentTime;
    private int currentTime {
        set {
            _currentTime = value;

            if (onCurrentTimeWasChanged != null) {
                onCurrentTimeWasChanged(_currentTime);
            }
        }
        get {
            return _currentTime;
        }
    }

    public System.Action onTimeOut;
    public System.Action<int> onCurrentTimeWasChanged;

    private void Awake() {
        currentTime = timeForLevel;
        StartCoroutine(cr_tick());
    }

    private IEnumerator cr_tick() {
        while (currentTime > 0) {
            yield return new WaitForSeconds(1f);
            currentTime -= 1;
        }

        if (onTimeOut != null) {
            onTimeOut();
        }
    }
}