﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FieldGenerator : SingleTon<FieldGenerator>
{
    [SerializeField] private Control control;
    [Space]
    [SerializeField] private Transform centerPoint;
    [SerializeField] public float step;
    [SerializeField] private Bubble bubblePrefub;
    [Space]
    [SerializeField] private int startWidth;
    [SerializeField] private int startHeight;

    // Temp variables for SpawnBubles method
    private Vector2 tmpVector;
    private Bubble tmpBuble;

    private void Awake() {
        SpawnBubles(startHeight, startHeight);
    }

    // Create grid with bubles
    public void SpawnBubles(int width, int height) {
        int xName = 1;
        int yName = 1;

        for (float x = centerPoint.position.x - (step * ((float)width / 2f)); x < centerPoint.position.x + (step * ((float)width / 2f)); x += step) {
            for (float y = centerPoint.position.y - (step * ((float)height / 2f)); y < centerPoint.position.y + (step * ((float)height / 2f)); y += step) {
                tmpVector.x = x + step / 2;
                tmpVector.y = y + step / 2;

                tmpBuble = Instantiate(bubblePrefub, tmpVector, Quaternion.identity).GetComponent<Bubble>();
                tmpBuble.transform.SetParent(transform);
                tmpBuble.gameObject.name = "x: " + xName + "y: " + yName;

                yName++;
            }

            xName++;
            yName = 1;
        }
    }
}
