﻿using System;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

public class Bubble : MonoBehaviour {
    public SpriteRenderer sr;
    [SerializeField] private Colors colors;
    [Space]
    [Header("Neightbours")]
    [ShowOnly]
    public Bubble left;
    [ShowOnly]
    public Bubble top;
    [ShowOnly]
    public Bubble right;
    [ShowOnly]
    public Bubble bottom;
    [Space]
    [ShowOnly]
    public bool bubbleSelected = false;

    [ShowOnly]
    public string colorName;

    [SerializeField] private Bubble bubblePrefub;

    [HideInInspector]
    public BubbleAnimations bubbleAnimations;

    private void Awake() {
        if (colors != null && colors.colors.Count > 0) {
            ColorWithName col = colors.GetRandomColor();

            sr.color = col.color;
            colorName = col.name;
        } else {
            Debug.LogError("Настройте и укажите коллекцию цветов у шарика!");
        }
    }

    public void Start() {
        SetNeightbours();
    }

    public bool isSelf(Bubble bubble) {
        return Equals(bubble);
    }

    public void Hide() {
        if (gameObject != null) {
            StartCoroutine(cr_Destroy_Delay());
        }
    }

    public bool HasNeightbourWithSameColor() {
        return left.colorName == colorName || right.colorName == colorName || top.colorName == colorName || bottom.colorName == colorName;
    }

    public bool isBubbleNeightbour(Bubble bubble) {
        return  (left != null && bubble.Equals(left)) || 
                (right != null && bubble.Equals(right)) || 
                (top != null && bubble.Equals(top)) || 
                (bottom != null && bubble.Equals(bottom));
    }

    private GameObject tmpGameObject;

    public IEnumerator cr_Destroy_Delay() {
        bubbleAnimations.currentState = BubbleAnimations.BubbleStates.destroy;
        yield return new WaitForSeconds(0.2f);
        tmpGameObject = Instantiate(bubblePrefub.gameObject, transform.position, transform.rotation);
        tmpGameObject.transform.position = transform.position;
        tmpGameObject.transform.rotation = transform.rotation;
        tmpGameObject.transform.SetParent(transform.parent);
        tmpGameObject.name = gameObject.name;
        Destroy(gameObject);
    }

    private Vector3 tmpPos;

    public void SetNeightbours() {
        tmpPos = transform.position;
        tmpPos.x -= FieldGenerator.Instance.step;

        Collider2D col = Physics2D.OverlapPoint(tmpPos);

        if (col != null && col.GetComponent<Bubble>()) {
            left = col.GetComponent<Bubble>();
            col.GetComponent<Bubble>().right = this;
        }

        tmpPos = transform.position;
        tmpPos.x += FieldGenerator.Instance.step;

        col = Physics2D.OverlapPoint(tmpPos);

        if (col != null && col.GetComponent<Bubble>()) {
            right = col.GetComponent<Bubble>();
            col.GetComponent<Bubble>().left = this;
        }

        tmpPos = transform.position;
        tmpPos.y += FieldGenerator.Instance.step;

        col = Physics2D.OverlapPoint(tmpPos);

        if (col != null && col.GetComponent<Bubble>()) {
            top = col.GetComponent<Bubble>();
            col.GetComponent<Bubble>().bottom = this;
        }

        tmpPos = transform.position;
        tmpPos.y -= FieldGenerator.Instance.step;

        col = Physics2D.OverlapPoint(tmpPos);

        if (col != null && col.GetComponent<Bubble>()) {
            bottom = col.GetComponent<Bubble>();
            col.GetComponent<Bubble>().top = this;
        }
    }

    public void OnDrawGizmos() {
#if UNITY_EDITOR
        Handles.Label(transform.position, gameObject.name);
#endif
    }
}