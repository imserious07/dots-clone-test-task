﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scores : MonoBehaviour
{
    public const string HIGH_SCORES_NAME = "highScores";

    [SerializeField] private LinesController linesControler;

    [ShowOnly]
    [SerializeField]private int _highScores = 0;
    private int highScores {
        set {
            _highScores = value;
            PlayerPrefs.SetInt(HIGH_SCORES_NAME, _highScores);
        }
        get {
            if(_highScores == 0) {
                if (PlayerPrefs.HasKey(HIGH_SCORES_NAME)) {
                    highScores = PlayerPrefs.GetInt(HIGH_SCORES_NAME);
                } else {
                    highScores = 0;
                }
            }

            return _highScores;
        }
    }

    public System.Action<int> onScoresWasChanged;

    [ShowOnly]
    [SerializeField] private int _currentScores;
    private int currentScores {
        set {
            _currentScores = value;

            if (onScoresWasChanged != null) {
                onScoresWasChanged(_currentScores);
            }
        }
        get {
            return _currentScores;
        }
    }

    private LoseTimer loseTimer;

    public int GetCurrentScores() {
        return currentScores;
    }

    public int GetHighScores() {
        return highScores;
    }

    public void ChangeHighScores() {
        if (currentScores > highScores) {
            highScores = currentScores;
        }
    }

    private void OnTimesOut() {
        ChangeHighScores();
    }

    private void Awake() {
        loseTimer = FindObjectOfType<LoseTimer>();

        if (loseTimer != null) {
            loseTimer.onTimeOut += OnTimesOut;
        }

        linesControler.onBubblesExplode += onBubbleExplode;
    }

    private void OnDestroy() {
        linesControler.onBubblesExplode -= onBubbleExplode;

        if (loseTimer != null) {
            loseTimer.onTimeOut -= OnTimesOut;
        }
    }

    private void onBubbleExplode(List<Bubble> bubbles) {
        if (bubbles.Count > 1) {
            currentScores += bubbles.Count * (10 + bubbles.Count);
        }
    }
}
