﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Control : MonoBehaviour
{
    [SerializeField] private Camera mainCamera;

    [HideInInspector]
    private Bubble tmpBubble;

    public System.Action<Bubble> onTouchBubble;
    public System.Action<Bubble> onMovedBubble;
    public System.Action<Bubble> onReleseBubble;
    public System.Action<Bubble> onEnterBubble;
    public System.Action<Bubble> onLeaveBubble;

    public System.Action onRelese;

    private Bubble currentBubble = null;

    // Update is called once per frame
    void Update()
    {
        if (Application.platform == RuntimePlatform.WindowsEditor) {
            SendInputActionMouse();
        }

        SendInputActionAndroid(TouchPhase.Began, onTouchBubble);
        SendInputActionAndroid(TouchPhase.Moved, onMovedBubble);
        SendInputActionAndroid(TouchPhase.Ended, onReleseBubble);
    }

    private void SendInputActionMouse() {
        if (Input.GetMouseButtonDown(0) && EventSystem.current.currentSelectedGameObject == null) {
            InvokeAction(onTouchBubble, Input.mousePosition, true);
        }

        if (Input.GetMouseButton(0) && EventSystem.current.currentSelectedGameObject == null) {
            InvokeAction(onMovedBubble, Input.mousePosition, true);
        }

        if (Input.GetMouseButtonUp(0) && EventSystem.current.currentSelectedGameObject == null) {
            InvokeAction(onReleseBubble, Input.mousePosition, false);
        }
    }

    private void SendInputActionAndroid(TouchPhase phase, System.Action<Bubble> action) {
        if ((Input.touchCount > 0) && (Input.GetTouch(0).phase == phase) && EventSystem.current.currentSelectedGameObject == null) {
            InvokeAction(action, Input.GetTouch(0).position, true);
        }
    }

    private void InvokeAction(System.Action<Bubble> action, Vector2 screenPoint, bool isPressed) {
        RaycastHit2D raycastHit = Physics2D.Raycast(mainCamera.ScreenToWorldPoint(screenPoint), Vector2.zero);
        if (raycastHit.point != Vector2.zero && raycastHit.collider != null) {
            tmpBubble = raycastHit.collider.GetComponent<Bubble>();

            if (tmpBubble != null) {
                if (action != null) {
                    action(tmpBubble);
                }

                if (currentBubble == null || currentBubble != tmpBubble) {
                    if (isPressed && onEnterBubble != null && EventSystem.current.currentSelectedGameObject == null) {
                        onEnterBubble(tmpBubble);
                    }

                    currentBubble = tmpBubble;
                }

                // Test
                //if (Debug.isDebugBuild) {
                //    if (action.Equals(onTouchBubble))
                //        print("onTouch");
                //    if (action.Equals(onMovedBubble))
                //        print("onMovedBubble");
                //    if (action.Equals(onReleseBubble))
                //        print("onReleseBubble");
                //}
            } else {
                if (action != null) {
                    action(null);
                }

                if (currentBubble != null) {
                    if (isPressed && onLeaveBubble != null && EventSystem.current.currentSelectedGameObject == null) {
                        onLeaveBubble(tmpBubble);
                    }

                    currentBubble = null;
                }
            }
        } else {
            if (action != null) {
                action(null);
            }
        }
    }
}
