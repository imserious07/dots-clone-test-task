﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LinesController : MonoBehaviour
{
    [SerializeField]private Control control;

    [ShowOnly]
    [SerializeField]private List<Bubble> bubbles = new List<Bubble>();

    [ShowOnly]
    [SerializeField]public string startColor;

    [Space]
    [Header("Lines prefubs")]
    [SerializeField] private Line leftToRight;
    [SerializeField] private Line rightToLeft;
    [SerializeField] private Line topToDown;
    [SerializeField] private Line downToTop;

    private List<Line> lines = new List<Line>();

    public System.Action<List<Bubble>> onBubblesExplode;

    private void Awake() {
        control.onEnterBubble += onEneterBubble;
        control.onReleseBubble += onReleseBubble;
    }

    private void OnDestroy() {
        control.onEnterBubble -= onEneterBubble;
        control.onReleseBubble -= onReleseBubble;
    }

    public void onEneterBubble(Bubble bubble) {
        if (bubble != null) {
            print("onEneterBubble");
            AddBubble(bubble);
        }
    }

    public void createLine(Bubble preBubble, Bubble nextBubble, Color color) {
        if (preBubble != null && nextBubble != null) {
            Vector3 AB = nextBubble.transform.position - preBubble.transform.position;
            Vector3 middle = preBubble.transform.position + ((Vector3.Distance(nextBubble.transform.position, preBubble.transform.position) / 2) * AB.normalized);

            Line lineTMP = null;

            if (nextBubble.Equals(preBubble.right)) {
                lineTMP = Instantiate(leftToRight.gameObject, middle, Quaternion.identity).GetComponent<Line>();
            } else if (nextBubble.Equals(preBubble.left)) {
                lineTMP = Instantiate(rightToLeft.gameObject, middle, Quaternion.identity).GetComponent<Line>();
            } else if (nextBubble.Equals(preBubble.top)) {
                lineTMP = Instantiate(downToTop.gameObject, middle, Quaternion.identity).GetComponent<Line>();
            } else if (nextBubble.Equals(preBubble.bottom)) {
                lineTMP = Instantiate(topToDown.gameObject, middle, Quaternion.identity).GetComponent<Line>();
            }

            if (lineTMP != null) {
                lineTMP.sr.color = color;
            }

            lines.Add(lineTMP);
        }
    }

    public void onReleseBubble(Bubble bubble) {
        LineFinished();
        bubbles.Clear();
    }

    public void LineFinished() {
        if (bubbles.Count > 1) {
            if (onBubblesExplode != null) {
                onBubblesExplode(bubbles);
            }

            foreach (Bubble bubble in bubbles) {
                if (bubble != null) {
                    bubble.Hide();
                }
            }

            foreach (Line l in lines) {
                if (l != null) {
                    Destroy(l.gameObject);
                }
            }

            lines.Clear();

            StartCoroutine(cr_checkLines());
        } else if (bubbles.Count == 1) {
            bubbles[0].bubbleAnimations.currentState = BubbleAnimations.BubbleStates.grow_down;
        }
    }

    private IEnumerator cr_checkLines() {
        yield return new WaitForSeconds(0.2f);
        CheckForLinesOnField();
    }

    private Bubble[] allBubles;
    private void CheckForLinesOnField() {
        allBubles = FindObjectsOfTypeAll(typeof(Bubble)) as Bubble[];

        foreach (Bubble b in allBubles) {
            if (b.HasNeightbourWithSameColor()) {
                return;
            }
        }

        for (int i = 0; i < allBubles.Length; i++) {
            allBubles[i].Hide();
        }

        allBubles = null;
    }

    // Add bubble to line of bubbles if possible
    public void AddBubble(Bubble bubble) {
        if (bubble != null && bubbles.Contains(bubble) == false) {
            if (bubbles.Count > 0 && 
                bubbles.Contains(bubble) == false &&
                bubble.isBubbleNeightbour(bubbles[bubbles.Count - 1]) && 
                bubble.colorName.Equals(startColor)) {
                bubbles.Add(bubble);
                bubble.bubbleAnimations.currentState = BubbleAnimations.BubbleStates.grow_up;

                createLine(bubbles[bubbles.Count - 2], bubbles[bubbles.Count - 1], bubble.sr.color);

            } else if (bubbles.Count == 0) {
                bubbles.Add(bubble);
                startColor = bubble.colorName;
                bubble.bubbleAnimations.currentState = BubbleAnimations.BubbleStates.grow_up;
            }
        }
    }
}
